REVISION := $(shell git rev-parse --short=8 HEAD || echo unknown)
BRANCH := $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)

LATEST_STABLE_TAG := $(shell git tag -l "*.*.*_*.*-*_*" --sort=-v:refname | head -n 1)
ifeq ($(shell git describe --exact-match --match $(LATEST_STABLE_TAG) >/dev/null 2>&1; echo $$?), 0)
IS_LATEST := 1
endif

CI_REGISTRY ?= registry.gitlab.com
CI_REGISTRY_IMAGE ?= registry.gitlab.com/tmaczukin-docker/alpine-bash
CI_COMMIT_REF_SLUG ?= $(BRANCH)

IMAGE_LATEST ?= $(CI_REGISTRY_IMAGE):latest
IMAGE ?= $(CI_REGISTRY_IMAGE):$(CI_COMMIT_REF_SLUG)

.PHONY: info
info:
	# IS_LATEST: $(IS_LATEST)

.PHONY: build
build:
	@docker pull $(IMAGE_LATEST) || echo "No $(IMAGE_LATEST) - will not use cache"
	@docker build --cache-from $(IMAGE_LATEST) -t $(IMAGE) .
ifdef IS_LATEST
	@docker tag $(IMAGE) $(IMAGE_LATEST)
endif

.PHONY: publish
publish:
	@docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	@docker push $(IMAGE)
ifdef IS_LATEST
	@docker push $(IMAGE_LATEST)
endif
	@docker logout $(CI_REGISTRY)

